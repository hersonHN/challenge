'use strict';
var app = {};

// dependencies
var shiftVowels      = require('../functions/shiftVowels');
var sort             = require('../functions/sort');
var fibonacciReplace = require('../functions/fibonacciReplace');
var concatenate      = require('../functions/concatenate');

app.main = function (param, fibo) {
    param = this.shiftVowels(param);
    param = this.sortReverse(param);
    param = this.fibonacciReplace(param, fibo);
    param = this.concatenate(param);
    param = this.base64(param);
    
    return param;
};

app.shiftVowels = shiftVowels;
app.sortReverse = sort.sortReverse;
app.fibonacciReplace = fibonacciReplace;
app.concatenate = concatenate.asciiConcat;
app.base64 = concatenate.base64;

module.exports = app;

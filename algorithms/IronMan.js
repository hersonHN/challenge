'use strict';
var app = {};

// dependencies
var sort        = require('../functions/sort');
var shiftVowels = require('../functions/shiftVowels');
var concatenate = require('../functions/concatenate');

app.main = function (param) {
    param = this.sort(param);
    param = this.shiftVowels(param);
    param = this.concatenate(param);
    param = this.base64(param);

    return param;
};

app.sort = sort.sort;
app.shiftVowels = shiftVowels;
app.concatenate = concatenate.asciiConcat;
app.base64 = concatenate.base64;

module.exports = app;

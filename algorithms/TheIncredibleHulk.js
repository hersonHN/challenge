'use strict';
var app = {};

// dependencies
var sort        = require('../functions/sort');
var shiftVowels = require('../functions/shiftVowels');
var concatenate = require('../functions/concatenate');

app.main = function (param) {
    param = this.shiftVowels(param);
    param = this.sortReverse(param);
    param = this.concatenate(param);
    param = this.base64(param);

    return param;
};

app.shiftVowels = shiftVowels;
app.sortReverse = sort.sortReverse;
app.concatenate = concatenate.asterisk;
app.base64 = concatenate.base64;

module.exports = app;

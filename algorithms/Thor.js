'use strict';
var app = {};

// dependencies
var splitWords       = require('../functions/splitWords');
var sort             = require('../functions/sort');
var switchCase       = require('../functions/switchCase');
var fibonacciReplace = require('../functions/fibonacciReplace');
var concatenate      = require('../functions/concatenate');

app.main = function (param, fibo) {
    param = this.splitWords(param);
    param = this.sort(param);
    param = this.switchCase(param);
    param = this.fibonacciReplace(param, fibo);
    param = this.concatenate(param);
    param = this.base64(param);

    return param;
};

app.splitWords = splitWords;
app.sort = sort.sort;
app.switchCase = switchCase;
app.fibonacciReplace = fibonacciReplace;
app.concatenate = concatenate.asterisk;
app.base64 = concatenate.base64;

module.exports = app;

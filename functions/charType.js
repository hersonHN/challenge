'use strict';

var app = {};
var vowels = { a: true, e: true, i: true, o: true, u: true, y: true };


app.isConsonant = function(char) {
    return (!app.isNumber(char) && !app.isVowel(char));
}

app.isNumber = function(char) {
    return !isNaN(char);
}

app.isVowel = function(char) {
    return (vowels[char.toLowerCase()] == true);
}

module.exports = app;

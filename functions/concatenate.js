'use strict';

var app = {};

app.asciiConcat = function (words) {
    var lastIndex = words.length - 1;

    var words = words.map(function (word, i) {
        var o = (i == 0) ? lastIndex : i - 1;
        word += words[o].charCodeAt(0);

        return word;
    });

    return words.join('');
};

app.asterisk = function (array) {
    return array.join('*');
};

app.base64 = function (str) {
    return (new Buffer(str)).toString('base64');
};

module.exports = app;

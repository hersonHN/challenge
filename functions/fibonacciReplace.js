'use strict';

var charType = require('./charType');

function fibonacciReplace(words, fibo) {
    var index = findFibonacciIndex(fibo);

    return words.map(function (word) {
        return word.split('').map(function (char) {
            if (charType.isVowel(char)) {
                char = fibonacci(index);
                index++;
            }
            return char;
        }).join('');
    });
}

function findFibonacciIndex(limit) {
    var index = 0;
    var current = fibonacci(index);

    while (true) {
        current = fibonacci(index);
        if (current >= limit) {
            return index;
        }

        index++;
    }
}

function fibonacci(n) {
    return Math.round(
        (
            Math.pow(0.5 + 0.5 * Math.sqrt(5.0), n) -
            Math.pow(0.5 - 0.5 * Math.sqrt(5.0), n)
        ) / Math.sqrt(5.0)
    );
}


module.exports = fibonacciReplace;

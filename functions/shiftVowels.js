'use strict';

var charType = require('./charType');

function shiftVowels(words) {
    return words.map(shiftVowelsFromWord);
}

function shiftVowelsFromWord (word) {
    var characters = word.split('');
    var lastIndex = characters.length - 1;
    var i, o, left, right, last;

    for (i = 0; i < characters.length; i++) {
        o = (i == lastIndex) ? 0 : i + 1;
        left  = characters[i];
        right = characters[o];

        if (!charType.isVowel(left)) continue;

        if (i == lastIndex) {
            last = characters.pop();
            characters.unshift(last);
        } else {
            characters[o] = left;
            characters[i] = right;
        }

        i++;
    }

    return characters.join('');
}


module.exports = shiftVowels;

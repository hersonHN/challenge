'use strict';

// dependencies
var _ = require('lodash');

var app = {};

app.sort = function (words) {
    // Memoize the function to run it just once time per word
    var strToAscii = _.memoize(stringToAsciiArray);

    // sort by ascii value:
    // numbers, then uppercase letters, then lowecase letters
    return words.sort(function (left, right) {
        left = strToAscii(left.toLowerCase());
        right = strToAscii(right.toLowerCase());

        var limit = left.length;
        for (var n = 0; n < limit; n++) {
            if (left[n] < right[n]) return -1;
            if (left[n] > right[n]) return 1;
        }

        return -1;
    });
};

function stringToAsciiArray(str) {
    var array = str.split('').map(function (char) {
        return char.charCodeAt(0);
    });
    return array;
}

app.sortReverse = function (array) {
    return app.sort(array).reverse();
};

module.exports = app;

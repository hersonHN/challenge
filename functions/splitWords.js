'use strict';

var _ = require('lodash');
var wordList = require('./words.json'); // json data

var delimiter = ' ';


function splitWords(words) {
    var list = words.map(split);
    list = _.flatten(list);
    return list;
}


function split(word) {
    var wordsFoundInside = [];
    wordList.forEach(function (w) {
        var index = word.toLowerCase().indexOf(w);
        var wordWithCase = word.substr(index, w.length);
        if (index > -1) {
            wordsFoundInside.push(wordWithCase);
        }
    })

    if (!wordsFoundInside.length) return word;

    var totalLength = wordsFoundInside.reduce(function (sum, wordFound) {
        var isFound = wordList.filter(function (w) { return w.toLowerCase() == wordFound.toLowerCase() }).length;
        if (!isFound) return sum;
        return sum + wordFound.length;
    }, 0);

    if (totalLength == word.length) {
        return wordsFoundInside;
    } else{
        return word;
    }

}


module.exports = splitWords;

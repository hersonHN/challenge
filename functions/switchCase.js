'use strict';

var charType = require('./charType');


var STATUS = {
    UPPER: 'uppercase',
    LOWER: 'lowercase',
}

function switchCase (words) {
    var charCase = caseStatus(words[0][0]);
    switchStatus(charCase);

    return words.map(function (w) {
        return switchCaseOfWord(w, charCase);
    });
}

function switchCaseOfWord(word, charCase) {
    return word
        .split('')
        .map(function (char) {
            return switchCaseOfChar(char, charCase);
        })
        .join('');
}

function switchCaseOfChar(char, charCase) {
    if (charType.isNumber(char)) return char;
    if (charType.isVowel(char)) return char;

    if (charCase.status == STATUS.UPPER) {
        char = char.toLowerCase();
    } else {
        char = char.toUpperCase();
    }
    switchStatus(charCase);

    return char;
}


function caseStatus(char) {
    return { status: (char == char.toUpperCase()) ? STATUS.UPPER : STATUS.LOWER }
}

function switchStatus(charCase) {
    if (charCase.status == STATUS.UPPER) {
        charCase.status = STATUS.LOWER;
    } else {
        charCase.status = STATUS.UPPER;
    }
}

module.exports = switchCase;

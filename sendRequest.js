
// dependencies
var request = require('request');
var UUID = require('uuid');
var Q = require('q');

var algos = {
    CaptainAmerica:    require('./algorithms/CaptainAmerica'),
    IronMan:           require('./algorithms/IronMan'),
    TheIncredibleHulk: require('./algorithms/TheIncredibleHulk'),
    Thor:              require('./algorithms/Thor')
};

var acklenUrl = base64Decode('aHR0cDovL2ludGVybmFsLWRldmNoYWxsZW5nZS0yLWRldi5hcHBoYi5jb20=');
var headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};


init();

function init() {
    var n = 20;
    while (n--) { sendRequest(); }
}


function sendRequest () {
    var formData = {
        name: 'Herson Salinas',
        emailAddress: base64Decode('aGVyc29uaG5AZ21haWwuY29t'),
        webhookUrl: base64Decode('aHR0cDovL21vcm5pbmctc3ByaW5ncy0xMTAxLmhlcm9rdWFwcC5jb20='),
        repoUrl: 'https://bitbucket.org/hersonHN/challenge'
    }

    var uuid = UUID.v1();

    getTheWords(uuid, formData)
    .then(function (data) {
        var result = processAlgorithm(data.body);

        data.myEncode = result.encoded;
        data.algo = data.body.algorithm;

        return sendDataToServer(data);
    })
    .then(function (data) {
        console.log(data);
    })
    .fail(function (error) {
        console.error(error.message);
    });
}


function getTheWords(uuid, formData) {
    var deferred = Q.defer();
    var url = buildURL();

    request({ url: url, headers: headers }, function (error, response, body) {
        if (error) {
            deferred.reject(new Error(error));
        }

        if (!error && response.statusCode == 200) {
            deferred.resolve({
                uuid: uuid,
                body: JSON.parse(body),
                formData: formData
            });
        }
    });

    return deferred.promise;

    function buildURL() {
        var urlToCall = [acklenUrl, 'values', uuid].join('/');
        return urlToCall;
    }
}


function processAlgorithm(params) {
    var algoName = params.algorithm;
    algo = algos[algoName];

    if (!algo) return console.error('invalid algorithm', algoName);
    var response = algo.main(params.words, params.startingFibonacciNumber);

    return { encoded: response, algo: algoName };
}


function sendDataToServer(data) {
    var deferred = Q.defer();
    var url = buildURL();

    var encodedValue = data.myEncode;
    var formData = data.formData;

    formData.encodedValue = encodedValue;

    request.post({
        url: url,
        form: formData,
        headers: headers
    }, function (error, response, body) {
        if (error) {
            deferred.reject(new Error(error));
        }

        if (!error && response.statusCode == 200) {
            deferred.resolve(body);
        }
    });

    return deferred.promise;


    function buildURL() {
        var urlToCall = [acklenUrl, 'values', data.uuid, data.algo].join('/');
        return urlToCall;
    }
}

function base64Decode(b64string) {
    return (new Buffer(b64string, 'base64')).toString('ascii');
}

function base64Encode(str) {
    return (new Buffer(str)).toString('base64');
}

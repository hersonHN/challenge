'use strict';

var CaptainAmerica = require('./algorithms/CaptainAmerica');
var Hulk           = require('./algorithms/TheIncredibleHulk');
var IronMan        = require('./algorithms/IronMan');
var Thor           = require('./algorithms/Thor');


function testIronMan() {
    console.log(IronMan.sort(['dog', 'cat', '5zebra', 'bird']));
    console.log(IronMan.shiftVowels(['hEllo', 'bOok', 'read', 'NeEd', 'paliNdromE', 'happy']));
    console.log(IronMan.concatenate(['dog', 'cat', 'bird']));
    console.log(IronMan.base64('dog98cat100bird99'));
}

function testHulk() {
    console.log(Hulk.shiftVowels(['hEllo', 'bOok', 'read', 'NeEd', 'paliNdromE', 'happy']));
    console.log(Hulk.sortReverse(['dog', 'cat', 'bird']));
    console.log(Hulk.concatenate(['dog', 'cat', 'bird']));
    console.log(Hulk.base64('dog*cat*bird'));
}

function testThor() {
    console.log(Thor.splitWords(['superman', 'gunshot','ThIsIsHaRd', 'WhAtArEyOuSmOkInG']));
    console.log(Thor.switchCase(['DoG', 'CaT', 'Bird']));
    console.log(Thor.fibonacciReplace(['dog', 'cat', 'bird'], 5));
    console.log(Thor.concatenate(['dog', 'cat', 'bird']));
    console.log(Thor.base64('dog98cat100bird99'));
}

function testCaptainAmerica() {
    console.log(CaptainAmerica.shiftVowels(['hEllo', 'bOok', 'read', 'NeEd', 'paliNdromE', 'happy']));
    console.log(CaptainAmerica.sortReverse(['dog', 'cat', 'bird']));
    console.log(CaptainAmerica.fibonacciReplace(['dog', 'cat', 'bird'], 5));
    console.log(CaptainAmerica.concatenate(['dog', 'cat', 'bird']));
    console.log(CaptainAmerica.base64('dog98cat100bird99'));
}

testIronMan();
testHulk();
testThor();
testCaptainAmerica();

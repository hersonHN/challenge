
// dependencies
var request = require('request');
var UUID = require('uuid');
var Q = require('q');

var algos = {
    CaptainAmerica:    require('./algorithms/CaptainAmerica'),
    IronMan:           require('./algorithms/IronMan'),
    TheIncredibleHulk: require('./algorithms/TheIncredibleHulk'),
    Thor:              require('./algorithms/Thor')
};


var acklenUrl = 'http://internal-devchallenge-2-dev.apphb.com';
var headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

var uuid = UUID.v1();

getTheWords(uuid)
.then(function (data) {
    var result = processAlgorithm(data.body);
    var myEncode = result.encoded;

    data.myEncode = myEncode;
    data.algo = data.body.algorithm;

    return checkAgainstServer(data);
})
.then(function (data) {
    var encoded = data.encoded;
    var myEncode = data.myEncode;
    var theirEncode = encoded;

    data.theirEncode = theirEncode;

    return data;
})
.then(function (data) {
    if (base64Decode(data.myEncode) == base64Decode(data.theirEncode)) {
        console.log('ok.');
    } else {
        console.error('ERROR');
        console.error(data);
    }
})
.fail(function (error) {
    console.error(error.message);
});




function getTheWords(uuid) {
    var deferred = Q.defer();
    var url = buildURL();

    request({ url: url, headers: headers }, function (error, response, body) {
        if (error) {
            deferred.reject(new Error(error));
        }

        if (!error && response.statusCode == 200) {
            deferred.resolve({
                uuid: uuid,
                body: JSON.parse(body)
            });
        }
    });

    return deferred.promise;

    function buildURL() {
        var urlToCall = [acklenUrl, 'values', uuid].join('/');
        return urlToCall;
    }
}


function processAlgorithm(params) {
    var algoName = params.algorithm;
    algo = algos[algoName];

    if (!algo) return console.error('invalid algorithm', algoName);
    var response = algo.main(params.words, params.startingFibonacciNumber);

    return { encoded: response, algo: algoName };
}


function checkAgainstServer(data) {
    var deferred = Q.defer();
    var url = buildURL();

    request({ url: url, headers: headers }, function (error, response, body) {
        if (error) {
            deferred.reject(new Error(error));
        }

        if (!error && response.statusCode == 200) {
            data.encoded = JSON.parse(body).encoded;
            deferred.resolve(data);
        }
    });

    return deferred.promise;

    function buildURL() {
        var urlToCall = [acklenUrl, 'encoded', data.uuid, data.algo].join('/');
        return urlToCall;
    }
}

function base64Decode(b64string) {
    return (new Buffer(b64string, 'base64')).toString('ascii');
}
